<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Route::get('/user', [UserController::class,'Has_One_Through']);

Route::view('/HasOne-Through', 'user');
Route::get('/one-to-one', [UserController::class,'index'])->name('one-to-one');
Route::get('/one-to-many', [UserController::class,'one_many'])->name('one-to-many');

Route::get('/Has-One-Through', [UserController::class,'Has_One_Through'])->name('Has-One-Through');
