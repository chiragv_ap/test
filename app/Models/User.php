<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;
    public function phone(){
        return $this->hasOne(Phone::class);
    }
    public function manyPhone(){
        return $this->hasMany(Phone::class);
    }
    // public  function has_one_through(){
    //     return $this->hasOneThrough(Order::class,Product::class,'phone_id','user_id','id','id');
    // }
}
