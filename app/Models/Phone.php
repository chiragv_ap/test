<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    use HasFactory;
    protected $hidden = ['created_at','updated_at'];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function getusers(){
        return $this->belongsToMany(User::class,'user_id');
    }
    public  function has_one_through(){
        // return $this->hasOneThrough(Order::class,Product::class,'phone_id','product_id','id','id');
        return $this->hasOneThrough(Order::class,Product::class);
    }
}
