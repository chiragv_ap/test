<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Phone;
use App\Models\Product;
use App\Models\Order;

use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        for ($i = 1; $i <= 19; $i++) {

            $faker = Faker::create();
            $user = new Order();
            // $user->product_name = $faker->randomElement(['product','camera','phone','cover','charging','computer','cpu','mouse','keyboard']);
            // $user->phone_id = $faker->unique()->numberBetween(987654321,9876543210);
            $user->product_id = $faker->unique()->numberBetween(1,19);

            $user->save();
        }
    }
}
